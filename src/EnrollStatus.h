/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef  ENROLL_STATUS_LABEL_
#define ENROLL_STATUS_LABEL_

#include <QLabel>
#include <QHBoxLayout>
#include <QWidget>
#include <QPixmap>

class EnrollStatus : public QWidget {
	Q_OBJECT

	private:
		QHBoxLayout	 *hb;
		QLabel		*fingerprint;
		QLabel		*status;
		QPixmap		*okPix;
		QPixmap		*errPix;
		bool		state;
		void		initComponents();
		void		loadImages(QString fp, QString enOk, QString enEr);
		void		setConstraints();
	public:
		EnrollStatus(QWidget *parent=0);
		EnrollStatus(QString fp, QString enOk=0, QString enEr=0, QWidget *parent=0);
		~EnrollStatus();
		void		setChecked(bool checked);
		void		clearStatus();
};

#endif