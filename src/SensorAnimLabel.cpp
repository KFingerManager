/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <KStandardDirs>

#include "SensorAnimLabel.h"

/**
 * SensorAnimLabel - label with swipe/place animation.
 * @param sensor sensor type for selecting right animation
 * @param parent parent widget
 */
SensorAnimLabel::SensorAnimLabel(QString sensor, QWidget *parent) : QLabel(parent) {
	if (sensor == "swipe") {
		movie = new QMovie(KStandardDirs::locate("data", "kfingermanager/pics/swipe.gif"));
	} else {
		// TODO get some animation for non-swipe sensors
		movie = new QMovie(KStandardDirs::locate("data", "kfingermanager/pics/swipe.gif"));
	}

	movie->start();
	movie->setPaused(true);
	setMovie(movie);
	setFixedSize(minimumSizeHint());
	clear();
}

/**
 * SensorAnimLabel destructor.
 */
SensorAnimLabel::~SensorAnimLabel() {
	movie->stop();
	clear();
	delete(movie);
}

/**
 * start - start animation playback.
 */
void SensorAnimLabel::start() {
	setMovie(movie);
	movie->setPaused(false);
}

/**
 * stop - stops animation playback
 */
void SensorAnimLabel::stop() {
	movie->setPaused(true);
	clear();
}
