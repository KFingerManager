/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <KLocale>
#include <QtCore>
#include <QFont>

#include "FMEnroll.h"
#include "FingerNames.h"

/**
 * getIface - create new D-Bus interface object, reads scan type and number of
 * enroll stages.
 */
void FMEnroll::getIface() {
	enrollStages = 0;
	dbc->claimDevice(device_.device, login_);
	if (dbc->isDeviceClaimed()) {
		enrollStages = dbc->getEnrollStages();
		scanType = dbc->getScanType();
		QObject::connect(dbc, SIGNAL(EnrollStatus(QString,bool)),
			this, SLOT(enrollStatus(QString,bool)));
	} else {
		scanType = "";
	}
}

/**
 * FMEnroll constructor
 * @param device device selected
 * @param login users login
 * @param finger selected finger
 * @param parent parent widget
 */
FMEnroll::FMEnroll(DeviceModel device, QString login, int finger, QWidget *parent) :
	KDialog(parent), finger_(finger), device_(device), actualStage(0),
	login_(login) {
	// Modal dialog, only Cancel button, precedent by separator
	setModal(true);
	showButtonSeparator(true);
	setButtons(KDialog::Cancel);
	
	// D-Bus connection
	dbc = FMDbusConn::getConn();
	getIface();
	
	// Init and traslate UI components
	initComponents(enrollStages);
	retranslate();
	animLabel->stop();
	setMainWidget(mainPanel);
	QObject::connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
	
	// Layout
	statusLayout->addWidget(statusLabel);
	statusLayout->addWidget(actualStatus);
	statusLayout->addStretch();
	mainLayout->addLayout(statusLayout);
	
	stagesLayout->addStretch();
	for (int i = 0; i < enrollStages; i++) {
		stagesLayout->addWidget(stages[i]);
	}
	stagesLayout->addStretch();
	stagesLayout->addWidget(animLabel);
	
	mainLayout->addLayout(stagesLayout);
	mainPanel->setLayout(mainLayout);
	
	setMaximumSize(minimumSizeHint());
	setMinimumSize(minimumSizeHint());

	if (!dbc->isDeviceClaimed()) {
		actualStatus->setText(i18n("Error while claiming device."));
	} else {
		dbc->enrollStart(fingerNames[finger_].iface);
		animLabel->start();
	}
}

/**
 * initCimponents - init all widgets
 * @param enrollStages number of enroll stages
 */
void FMEnroll::initComponents(int enrollStages) {
	mainPanel = new QWidget();
	statusLabel = new QLabel();
	QFont font = statusLabel->font();
	font.setWeight(QFont::Bold);
	statusLabel->setFont(font);
	actualStatus = new QLabel();
	statusLayout = new QHBoxLayout();
	stagesLayout = new QHBoxLayout();
	mainLayout = new QVBoxLayout();
	timer = new QTimer();
	timer->setSingleShot(true);
	
	animLabel = new SensorAnimLabel(scanType);
	
	stages = (EnrollStatus**)malloc(sizeof(EnrollStatus*)*enrollStages);
	for (int i = 0; i < enrollStages; i++) {
		stages[i] = new EnrollStatus(this);
	}
}

/**
 * retranslate - update all UI strings
 */
void FMEnroll::retranslate() {
	// Set Window Title
	setWindowTitle(i18n("Enrolling finger..."));
	
	// What's this
	setWhatsThis(i18n("Enroll finger window"));
	
	// Set status label
	statusLabel->setText(i18n("Status")+":");
	
	// Set stages tooltips
	for (int i = 0; i < enrollStages; i++) {
		QString msg = i18n("Stage %1").arg(i + 1);
		stages[i]->setToolTip(msg);
		stages[i]->setWhatsThis(i18n("Current enrollment progess..."));
	}
	
	// Status message
	timeout();
}

/**
 * setStatusMessage - set suitable status message for actual enroll status
 * @param result actual enroll status
 */
void FMEnroll::setStatusMessage(QString result) {
	int timeout = 2500;
	bool checked = false;
	if (result == "enroll-stage-passed") {
		checked = true;
		actualStatus->setText(i18n("Stage passed"));
		timeout = 1250;
	} else if (result == "enroll-swipe-too-short") {
		actualStatus->setText(i18n("Swipe too short"));
	} else if (result == "enroll-finger-not-centered") {
		actualStatus->setText(i18n("Finger not centered"));
	} else if (result == "enroll-remove-and-retry") {
		actualStatus->setText(i18n("Remove and retry"));
	} else if (result == "enroll-retry-scan") {
		actualStatus->setText(i18n("Retry scan"));
	}
	
	stages[actualStage]->setChecked(checked);
	animLabel->stop();
	
	timer->start(timeout);

	if (checked) {
		actualStage++;
	}
}

/**
 * enrollStatus - Slot for asynchronous enroll signals
 * @param result actual enroll status
 * @param done true if enroll process is completed, otherwise false
 */
void FMEnroll::enrollStatus(QString result, bool done) {
	qDebug() << "enrol result:" << result << ", done: " << done;

	if (done == true) {
		if (result == "enroll-completed") {
			actualStatus->setText(i18n("Enroll completed"));
			stages[actualStage]->setChecked(true);
			setButtons(KDialog::Ok);
			dbc->releaseDevice();
		} else if (result == "enroll-disconnected") {
			actualStatus->setText(i18n("Device disconected. Fingerprint is not enrolled."));
			QtConcurrent::run(dbc, &FMDbusConn::releaseDevice);
		} else if (result == "enroll-failed") {
			actualStatus->setText(i18n("Enroll failed."));
			QtConcurrent::run(dbc, &FMDbusConn::releaseDevice);
		} else if (result == "enroll-unknown-error") {
			actualStatus->setText(i18n("Unknown error."));
			QtConcurrent::run(dbc, &FMDbusConn::releaseDevice);
		}
		animLabel->stop();
	} else setStatusMessage(result);
}

/**
 * timeout - handle timeout and set proper message
 */
void FMEnroll::timeout() {
	// Create actual status message
	QString msg;
	if (scanType == "swipe") {
		msg = QString(i18n("Swipe your \"%1\" on \"%2\""));
	} else {
		msg = QString(i18n("Place your \"%1\" on \"%2\""));
	}
	msg = msg.arg(i18n(fingerNames[finger_].name), device_.name);
	actualStatus->setText(msg);
	stages[actualStage]->clearStatus();
	animLabel->start();
}

/**
 * FMEnroll destructor
 */
FMEnroll::~FMEnroll() {
	qDebug() << "FMEnroll destructor";

	QObject::disconnect(this, SLOT(enrollStatus(QString,bool)));
	
	if (statusLabel) delete statusLabel;
	if (actualStatus) delete actualStatus;
	if (statusLayout) delete statusLayout;
	if (stages) {
		for (int i = 0; i < enrollStages; i++) {
			if (stages[i]) delete stages[i];
		}
		free(stages);
	}
	if (animLabel) delete animLabel;
	if (stagesLayout) delete stagesLayout;
	if (mainLayout) delete mainLayout;
	if (mainPanel) delete mainPanel;
	if (dbc) {
		dbc->releaseDevice();
		dbc->releaseConn();
	}
}
