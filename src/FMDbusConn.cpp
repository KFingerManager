/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <QDBusConnectionInterface>
#include <QtCore>
#include <QObject>

#include "FMDbusConn.h"
#include "FingerManagerDevicesModel.h"

FMDbusConn*	conn;
int		ocount = 0; // number of opened connection

/**
 * Hides constructor
 */
FMDbusConn* FMDbusConn::getConn() {
	if (ocount == 0) {
		conn = new FMDbusConn();
	}
	ocount++;
// 	qDebug("Number of references: %d", ocount);
	return(conn);
}

/**
 * Hides destructor
 */
void FMDbusConn::releaseConn() {
	if (ocount > 0) {
		ocount--;
// 		qDebug("Number of references: %d", ocount);
	}
	if (ocount == 0){
// 		qDebug("Deleting instance");
		delete this;
	}
}

/**
 * FMDbusConn constructor
 */
FMDbusConn::FMDbusConn() : managerIface(0), claimedDevice(0), isEnrollStopped(true),
	releaseInProgress(false) {
	if (!QDBusConnection::systemBus().isConnected()) {
		qDebug("Cannot connect to D-Bus system bus.");
	}
};

/**
 * FMDbusConn destructor
 */
FMDbusConn::~FMDbusConn() {
	delete managerIface;
}

/**
 * Start D-Bus daemon (requires some retries)
 * @param retr maximum retries while waiting for fprintd D-Bus service start
 */
QDBusInterface* FMDbusConn::getManagerIface(int retr = INTERFACE_ACQUIRE_RETRY) {
	if (!retr) {
// 		qDebug("Manager interface doesn't found!");
		return(0);
	}
	
	if (managerIface && managerIface->isValid()) {
// 		qDebug("Returning existing manager interface");
		return(managerIface);
	}
	
	// Connect to Fprint manager on system bus
	QDBusConnection bus = QDBusConnection::systemBus();
	QDBusInterface *iface = new QDBusInterface("net.reactivated.Fprint",
				"/net/reactivated/Fprint/Manager",
				"net.reactivated.Fprint.Manager",
				bus, this);
	
	// Wait while D-Bus Fprintd daemon starts
	if (iface->isValid()) {
		managerIface = iface;
	} else {
		managerIface = getManagerIface(--retr);
	}
	return(managerIface);
}

/**
 * Get device communication interface
 * @param device device which interface is required
 * @return QDBusInterface
 */
QDBusInterface* FMDbusConn::getDeviceInterface(QString device) {
	QDBusInterface* i = new QDBusInterface("net.reactivated.Fprint",
		device,
		"net.reactivated.Fprint.Device",
		QDBusConnection::systemBus());
	return(i);
}

/**
 * Returns name associated with device
 * @param device device which name is required
 * @return QString
 */
QString FMDbusConn::getDeviceName(QString device) {
	QString name;
	QDBusInterface *i = getDeviceInterface(device);
	if (i->isValid()) {
		name = i->property("name").toString();
	} else {
		name = device;
	}
	delete(i);
	return(name);
}

/**
 * getDevices - get list of installed devices
 * @return FingerManagerDevicesModel
 */
FingerManagerDevicesModel* FMDbusConn::getDevices() {
	QDBusInterface *iface = getManagerIface();
	FingerManagerDevicesModel *model = new FingerManagerDevicesModel();
	
	if (iface && iface->isValid()) {
		qDebug("D-Bus interface acquired! :-)");
		QDBusReply<QList<QDBusObjectPath> > devices = iface->call("GetDevices");
		if (devices.isValid()) {
			foreach (QDBusObjectPath device, devices.value()) {
				model->addDevice(device.path(), getDeviceName(device.path()));
			}
		}
		if (model->rowCount(QModelIndex()) > 0) { // found at least one device
			QDBusReply<QDBusObjectPath> defaultDevice = iface->call("GetDefaultDevice");
			if (defaultDevice.isValid()) {
				model->setDefault(defaultDevice.value().path());
			}
		}
	} else {
		qDebug("D-Bus interface cannot be acquired! :-(");
	}
	return(model);
}

/**
 * getFingers - get list of enrolled fingerprints for user on selected device
 * @param device device
 * @param username username
 * @return QStringList
 */
QStringList FMDbusConn::getFingers(QString device, QString username) {
	QDBusInterface *iface = getDeviceInterface(device);
	QStringList result;
	if (iface && iface->isValid()) {
		QDBusReply<QStringList> r = iface->call("ListEnrolledFingers", username);
		if (r.isValid()) {
			result = r.value();
		} else {
			QDBusError e = iface->lastError();
			qDebug() << e.message();
		}
	}
	delete(iface);
	qDebug() << "  result:" << result;
	return(result);
}

/**
 * deleteAllEnrolledFingers - delete all enrolled fingers for user on selected device
 * @param device device
 * @param usernabe username
 * @return bool
 */
bool FMDbusConn::deleteAllEnrolledFingers(QString device, QString username) {
	QDBusInterface *iface = getDeviceInterface(device);
	bool retval = false;
	if (iface && iface->isValid()) {
		iface->call("DeleteEnrolledFingers", username);
		retval = true;
	}
	delete iface;
	return(retval);
}

/*******************************************************************************
 * Device informations
 */

/**
 * getEnrollStages - returns number of enroll stages for selected device
 * @return int
 */
int FMDbusConn::getEnrollStages() {
	int stages = 3;
	if (isDeviceClaimed()) {
		// QDBusInterface cannot access properties with dashes in name,
		// so access must be done in other way
		QDBusInterface *iface = new QDBusInterface("net.reactivated.Fprint",
				claimedDevice->path(),
				"org.freedesktop.DBus.Properties",
				QDBusConnection::systemBus());
		if (iface) {
			if (iface->isValid()) {
				QDBusReply<QVariant> r = iface->call("Get",
					"net.reactivated.Fprint.Device",
					"num-enroll-stages");
				int s = r.value().toInt();
				if (s > 0) {
					stages = s;
				}
			}
			delete iface;
		}
//		// This code should do work once will be QtDbus/glib-dbus/GObject repaired
// 		int s = claimedDevice->property("num-enroll-stages").toInt();
// 		qDebug("Device enroll stages: %d", s);
// 		if (s>0) {
// 			stages = s;
// 		};
	}
	return(stages);
}

/**
 * getScanType - return sensor type for selected device
 * @return QString
 */
QString FMDbusConn::getScanType() {
	QString type("swipe");
	if (isDeviceClaimed()) {
		QDBusInterface *iface = new QDBusInterface("net.reactivated.Fprint",
				claimedDevice->path(),
				"org.freedesktop.DBus.Properties",
				QDBusConnection::systemBus());
		if (iface) {
			if (iface->isValid()) {
				QDBusReply<QVariant> r = iface->call("Get",
					"net.reactivated.Fprint.Device",
					"scan-type");
				QString t = r.value().toString();
				qDebug() << "Device scan type:" << t;
				if (t.length() > 0) {
					type = t;
				}
			}
			delete iface;
		}
//		// This code should do work once will be QtDbus/glib-dbus/GObject repaired
// 		QString t = claimedDevice->property("scan-type").toString();
// 		qDebug() << "Device scan type:" << t;
// 		if (t.length() > 0) {
// 			type = t;
// 		}
	}
	return(type);
}

/*******************************************************************************
 * User enrollment
 */
/**
 * claimDevice - claim device for user
 * @param device device
 * @param username username
 */
void FMDbusConn::claimDevice(QString device, QString username) {
	if (!claimedDevice) {
		QDBusInterface *iface = getDeviceInterface(device);
		if (iface) {
			if (iface->isValid()) {
				QDBusMessage m = iface->call("Claim", username);
				if (m.type() == QDBusMessage::ErrorMessage) {
					qDebug() << "Claim device:" << m.errorMessage();
					delete(iface);
					claimedDevice = 0;
				} else {
					claimedDevice = iface;
					QObject::connect(iface, SIGNAL(EnrollStatus(QString,bool)),
						this, SLOT(enrollCallback(QString,bool)));
				}
			} else {
				qDebug() << "Error:"<< iface->lastError().message();
				claimedDevice = 0;
				delete(iface);
			}
		}
	} /*else {
		qDebug("Device is allready claimed!");
	}*/
}

/**
 * releaseDevice - release claimed device
 */
void FMDbusConn::releaseDevice() {
	if (claimedDevice && !releaseInProgress) {
		releaseInProgress = true;
		// no locking on shared variable releaseInProgress, should be ok
		if (claimedDevice->isValid()) {
			if (!isEnrollStopped) enrollStop();
			QDBusMessage m = claimedDevice->call("Release");
			if (m.type() == QDBusMessage::ErrorMessage) {
				qDebug() << "  Release device:" << m.errorMessage();
			}
		}
		delete(claimedDevice);
		claimedDevice = 0;
	}
}

/**
 * isDeviceClaimed
 * @return bool true if is device claimed, otherwise false
 */
bool FMDbusConn::isDeviceClaimed() {
	if (claimedDevice && claimedDevice->isValid()) {
		return(true);
	} else {
		return(false);
	}
}

/**
 * enrollCallback - forward message from fprintd D-Bus service
 * @param status enroll status
 * @param result enroll result 
 */
void FMDbusConn::enrollCallback(QString status, bool result) {
	emit EnrollStatus(status, result);
}

/**
 * enrollStart - start enrollment for selected finger
 * @param finger selected finger
 */
void FMDbusConn::enrollStart(QString finger) {
	if (isDeviceClaimed()) {
		if (isEnrollStopped) {
			isEnrollStopped = false;
			claimedDevice->call("EnrollStart", finger);
		}/* else {
			qDebug("  Enrollment allready started...");
		}*/
	}
}

/**
 * enrollStop - stop enrollment
 */
void FMDbusConn::enrollStop() {
	if (isDeviceClaimed()) {
		if (!isEnrollStopped) {
			isEnrollStopped = true;
			claimedDevice->call("EnrollStop");
		}/* else {
			qDebug("  Enrollment allready stopped...");
		}*/
	}
}
