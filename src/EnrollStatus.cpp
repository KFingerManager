/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <KStandardDirs>
#include <KGlobal>

#include "EnrollStatus.h"

/**
 * EnrollStatus - Widget with big fingeprint image and small status icon.
 * @param fp path to fingerprint image
 * @param enOk path to enroll ok status icon
 * @param enEr path to enroll error status icon
 * @param parent parent widget
 */
EnrollStatus::EnrollStatus(QString fp, QString enOk, QString enEr, QWidget *parent) :
	QWidget(parent), state(false) {

	initComponents();
	loadImages(fp, enOk, enEr);
	setConstraints();
}

/**
 * EnrollStatus - Widget with big fingerprint image and small status icon.
 * This constructor uses predefined images. If you want define your own images,
 * use @see EnrollStatus(QString fp, QString enOk, QString enEr, QWidget *parent).
 * @param parent parent widget
 */
EnrollStatus::EnrollStatus(QWidget *parent) : QWidget(parent), state(false) {
	initComponents();
	loadImages(KStandardDirs::locate("data", "kfingermanager/pics/fingerprint.png"),
		   KStandardDirs::locate("data", "kfingermanager/pics/fp-ok.png"),
		   KStandardDirs::locate("data", "kfingermanager/pics/fp-error.png"));
	setConstraints();
}

/**
 * setConstraints - set fixed width for whole widgetand for status label.
 */
void EnrollStatus::setConstraints() {
	status->setPixmap(*okPix);
	status->setFixedWidth(status->minimumSizeHint().width());
	setFixedSize(minimumSizeHint());
	clearStatus();
}

/**
 * loadImages - load images needed by widget.
 * @param fp path to fingerprint image
 * @param enOk path to enroll ok status icon
 * @param enEr path to enroll error status icon
 */
void EnrollStatus::loadImages(QString fp, QString enOk, QString enEr) {
	
	fingerprint->setPixmap(QPixmap(fp));
	okPix = new QPixmap(enOk);
	errPix = new QPixmap(enEr);
}

/**
 * initComponents - initialize all components of widget.
 */
void EnrollStatus::initComponents() {
	hb = new QHBoxLayout(this);
	fingerprint = new QLabel();
	status = new QLabel();

	fingerprint->setAlignment(Qt::AlignBottom);
	status->setAlignment(Qt::AlignBottom);

	hb->addWidget(fingerprint);
	hb->addWidget(status);
	
	setLayout(hb);
}

/**
 * setChecked - set status icon.
 * @param ok True means use enroll ok status icon, false means use enroll error
 *	status icon
 */
void EnrollStatus::setChecked(bool ok) {
	state = ok;
	if (ok) {
		status->setPixmap(*okPix);
	} else {
		status->setPixmap(*errPix);
	}
}

/**
 * clearStatus - cleans status icon. Status icon will be cleaned only if status
 * icon is error status icon.
 */
void EnrollStatus::clearStatus() {
	if (!state) {
		status->clear();
	}
}

/**
 * ~EnrollStatus - destructor.
 */
EnrollStatus::~EnrollStatus() {
	delete(status);
	delete(fingerprint);
	delete(hb);
}