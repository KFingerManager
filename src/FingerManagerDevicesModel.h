/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef FINGERMANAGER_DEVICES_MODEL_H_
#define FINGERMANAGER_DEVICES_MODEL_H_

#include <QAbstractListModel>
#include <QVariant>
#include <QModelIndex>
#include <QString>
#include <QDBusObjectPath>

typedef struct {
	QString		name;
	QString		device;
} DeviceModel;

class FingerManagerDevicesModel : public QAbstractListModel {

	Q_OBJECT
	
	private:
		QList<DeviceModel> devices;
		int		default_;
	public:
		FingerManagerDevicesModel(QObject *parent = 0);
		~FingerManagerDevicesModel();
		
		int		rowCount(const QModelIndex&) const;
		QVariant	data(const QModelIndex& index, int role = Qt::DisplayRole) const;

		void		addDevice(QString device, QString name);
		void		setDefault(QString device);

		QString		getDeviceName(int row);
		QString		getDevicePath(int row);
		DeviceModel	getDeviceModel(int row);

		int		getDefault();
};

#endif