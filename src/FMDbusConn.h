/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef FMDBUSCONN_H_
#define FMDBUSCONN_H_

#include <QObject>
#include <QDBusInterface>

#include "FingerManagerDevicesModel.h"

#define INTERFACE_ACQUIRE_RETRY 3

class FMDbusConn : public QObject {
	
	Q_OBJECT
	
	signals:
		void EnrollStatus(QString status, bool result);
	private slots:
		void enrollCallback(QString status, bool result);
	private:
		QDBusInterface*			getManagerIface(int retr);
		QDBusInterface* 		getDeviceInterface(QString device);
		QString				getDeviceName(QString device);
		void				enrollStop();
		QDBusInterface			*managerIface;
		QDBusInterface			*claimedDevice;
		bool				isEnrollStopped;
		bool				releaseInProgress;
		FMDbusConn();
		~FMDbusConn();
	public:
		static FMDbusConn*		getConn();
		void 				releaseConn();
		FingerManagerDevicesModel*	getDevices();
		QStringList			getFingers(QString device, QString username);
		bool				deleteAllEnrolledFingers(QString device, QString username);
		void				claimDevice(QString device, QString username);
		void				releaseDevice();
		int				getEnrollStages();
		bool				isDeviceClaimed();
		void				enrollStart(QString finger);
		QString				getScanType();
};

#endif /*FMDBUSCONN_H_*/
