/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <KConfig>
#include <KConfigGroup>
#include <KLocale>
#include <KSeparator>
#include <QWidget>
#include <QPixmap>
#include <QButtonGroup>
#include <QSize>
#include <QtCore>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <KPushButton>
#include <KMessageBox>
#include <KPluginFactory>
#include <KPluginLoader>

#include "FingerManagerWindow.h"
#include "FingerManagerDevicesModel.h"
#include "FMDbusConn.h"
#include "FingerNames.h"
#include "FMEnroll.h"

int	y_base;

K_PLUGIN_FACTORY(FingerManagerWindowFactory,
	registerPlugin<FingerManagerWindow>();
	)

K_EXPORT_PLUGIN(FingerManagerWindowFactory("kcmkfingermanager"))

const static char fingerManagerVersion[] = "v0.0.5";

/**
 * Handle checkbox status changes
 * @param idx selected finger index
 */
void FingerManagerWindow::fingerSelected(int idx) {
	int devid = devices->currentIndex();
	if (devid > -1) {
		qDebug("%s: %d", fingerNames[idx].iface, fingers[idx]->isChecked());
		if (fingers[idx]->isChecked()) {
			KMessageBox::sorry(this, i18n("Deletion of single "
				"fingerprint is not supported in fprintd D-Bus "
				"daemon"));
			fingers[idx]->setChecked(true);
		} else {
			FMEnroll *e = new FMEnroll(
				((FingerManagerDevicesModel*)devices->model())->getDeviceModel(devid),
				login,
				idx,
				this);
			int retval = e->exec();
			delete(e);
			if (retval == QDialog::Rejected) {
				fingers[idx]->setChecked(false);
			}
		}
	}
}

/**
 * Retrieves enrolled fingers from dbus and set checked corresponding checkboxes
 * @param fngrs list of enrolled fingers
 */
void FingerManagerWindow::processFingers(QStringList fngrs) {
	qDebug("Enrolled fingers were discovered (%d)", fngrs.size());
	for (int i = 0; i < 10; i++) {
		if (fingers[i]) fingers[i]->setChecked(false);
	}
	for (int i = 0; i < fngrs.size(); i++) {
		QString finger = fngrs.at(i);
		for (int j = 0; j < 10; j++) {
			if (finger == fingerNames[j].iface) {
				if (fingers[j]) fingers[j]->setChecked(true);
			}
		}
	}
}

void FingerManagerWindow::enableAllFingers(bool enable) {
	for (int i = 0; i < MAX_FINGERS; i++) {
		if (fingers[i]) fingers[i]->setEnabled(enable);
	}
}

/**
 * Hook after search enrolled fingerprints for selected device
 * @param idx new selected device
 */
void FingerManagerWindow::deviceChanged(int idx) {
	qDebug("deviceChanged: %d", idx);
	if (idx > -1) {
		qDebug() <<  "  device: "<< ((FingerManagerDevicesModel*)devices->model())->getDevicePath(idx);
		QStringList fingers = dbc->getFingers(
			((FingerManagerDevicesModel*)devices->model())->getDevicePath(idx), login);
		processFingers(fingers);
		deleteAllBtn->setEnabled(true);
		enableAllFingers(true);
	} else {
		deleteAllBtn->setEnabled(false);
		enableAllFingers(false);
	}
}

/**
 * Verify deletion of enrolled fingerprints on selected device for current user
 */
void FingerManagerWindow::deleteAll() {
	//get device
	int idx = devices->currentIndex();
	if (idx > -1) {
		// Warning message
		QString device = ((FingerManagerDevicesModel*)devices->model())->
				getDeviceName(idx);
		QString msg = QString(i18n("Do you really want delete all fingerprints "
					"on device \"%1\" for user \"%2\""));
		msg = msg.arg(device, login);
		// Show Yes/No message box
		int result = KMessageBox::questionYesNo(this, msg);
		if (result == KMessageBox::Yes) {
			qDebug() << "Deleting all enrolled fingerprints...";
			if (dbc->deleteAllEnrolledFingers(
				((FingerManagerDevicesModel*)devices->model())->getDevicePath(idx),
				login)) {
				deviceChanged(idx);
			};
		}
	}
}

/**
 * createCheckBox - create checkbox on selected coordinates with finger index
 * @param idx finger index
 * @param x x coordinate
 * @param y y coordinate
 * @return new checkbox
 */
QCheckBox* FingerManagerWindow::createCheckBox(int idx, int x, int y) {
	QCheckBox *cb = new QCheckBox(handsPanel);
	QSize size = cb->sizeHint();
	cb->setGeometry(QRect(x - (size.width()/2), 
			y_base + y - (size.height()/2),
			size.height(),
			size.height()));
	btng->addButton(cb, idx);
	return(cb);
}

/**
 * loadConfig - config file loader
 * @param settings configuration settings
 */
void FingerManagerWindow::loadConfig(KConfig *settings) {
	// Config helper
	KConfigGroup cfgGroup;
	
	// Load image
	cfgGroup = settings->group("image");
	y_base = cfgGroup.readEntry("base", 20);
	
	QString filename = cfgGroup.readEntry("filename", "hands.png");
	QPixmap image = QPixmap(filename);
 	if (!image.isNull()) {
		handsLabel->setPixmap(image);
	} else {
		handsLabel->setText("File not found: " + filename);
	}
	// set label position
	handsLabel->setGeometry(QRect(0, y_base, image.width(), image.height()));

	// Set handsPanel dimension
	handsPanel->setMinimumWidth(handsLabel->width());
	handsPanel->setMinimumHeight(handsLabel->height());

	// Load checkboxes
	cfgGroup = settings->group("fingers");
	for (int i = 0; i < MAX_FINGERS; i++) {
		QString name;
		
		name.sprintf("%d/x", i+1);
		int x = cfgGroup.readEntry(name, 0);
		
		name.sprintf("%d/y", i+1);
		int y = cfgGroup.readEntry(name, 0);
		
		if (x && y) {
			qDebug("Adding new checkbox[%d]", i);
			fingers[i] = createCheckBox(i, x, y);
		} else {
			qDebug("Checkbox[%d] skipped", i);
			fingers[i] = 0;
		}
	}
	enableAllFingers(false);
}

/**
 * Actualize all texts in window after language changes
 */
void FingerManagerWindow::retranslate() {
	// Window title
	setWindowTitle(i18n("Finger manager..."));

	// What's this
	setWhatsThis(i18n("Finger Manager Application"));

	deviceLabel->setText(i18n("Select device:"));
	deleteAllBtn->setText(i18n("Delete all"));
	deleteAllBtn->setToolTip(i18n("Delete all enrolled fingerprints on selected device."));

	box->setTitle(i18n("Select finger for enroll or modify"));

	for (int i = 0; i < 10; i++) {
		if (fingers[i]) {
			fingers[i]->setWhatsThis(i18n(fingerNames[i].name));
			fingers[i]->setToolTip(i18n(fingerNames[i].name));
		}
	}
}

/**
 * FingerManagerWindow contructor
 * @param parent parent widget
 */
FingerManagerWindow::FingerManagerWindow(QWidget *parent, const QVariantList &) :
	KCModule(FingerManagerWindowFactory::componentData(), parent) {

	user = new KUser();

	setButtons(KCModule::NoAdditionalButton);

	about = new KAboutData("kfingermanager", "KFingerManager",
		ki18n("Manage Enrolled Fingers"), fingerManagerVersion,
		ki18n("Manage Enrolled Fingers of User"), KAboutData::License_GPL,
		ki18n("(c) 2009 Jaroslav Barton"), ki18n(0L));
	about->addAuthor(ki18n("Jaroslav Barton"), ki18n("Maintainer"), "djaara@djaara.net");
	setAboutData(about);

	login = user->loginName();

	// Prepare component
	initComponents();
	loadConfig(settings);
	retranslate();

	// Handle all check buttons
	btng->setExclusive(false);
	QObject::connect(btng, SIGNAL(buttonPressed(int)), SLOT(fingerSelected(int)));

	// Internal slots
	QObject::connect(devices, SIGNAL(currentIndexChanged(int)), this, SLOT(deviceChanged(int)));

	// Devices model for combo box
	FingerManagerDevicesModel* m = dbc->getDevices();
	devices->setModel(m);
	devices->setCurrentIndex(m->getDefault());

	// Handle delete all button
	QObject::connect(deleteAllBtn, SIGNAL(pressed()), this, SLOT(deleteAll()));

	// Layout
	handsLayout->addStretch();
	handsLayout->addWidget(handsPanel);
	handsLayout->addStretch();
	mainPanelLayout->addLayout(handsLayout);
	mainPanelLayout->addWidget(separator);
	devicesLayout->addWidget(deviceLabel);
	devicesLayout->addWidget(devices);
	devicesLayout->addStretch();
	devicesLayout->addWidget(deleteAllBtn);
	mainPanelLayout->addLayout(devicesLayout);
	mainLayout->addWidget(box);
	mainLayout->addStretch();

	setMinimumSize(box->minimumSizeHint());
}

/**
 * void initComponents()
 * Initialize main components of FingerManagerWindow
 */
void FingerManagerWindow::initComponents() {
	dbc = FMDbusConn::getConn();

	// config file
	settings = new KConfig("kfingerrc");

	// box around UI
	box = new QGroupBox();
	mainPanelLayout = new QVBoxLayout(box);

	// pane with hands image
	handsPanel = new QWidget();
	// and with buttons
	btng = new QButtonGroup(handsPanel);
	//and image
	handsLabel = new QLabel(handsPanel);

	// separator
	separator = new KSeparator(box);

	// device section
	deviceLabel = new QLabel();
	devices = new QComboBox();
	devices->setModel(new FingerManagerDevicesModel());
	deleteAllBtn = new KPushButton();
	deleteAllBtn->setEnabled(false);
	devicesLayout = new QHBoxLayout();
	devicesLayout->setSpacing(KDialog::spacingHint());
	devicesLayout->setMargin(0);

	mainLayout = new QVBoxLayout(this);
	mainLayout->setSpacing(KDialog::spacingHint());
	mainLayout->setMargin(0);

	handsLayout = new QHBoxLayout();

	for (int i = 0; i < MAX_FINGERS; i++) {
		fingers[i] = 0;
	}
}

/**
 * FingerManagerWindow destructor
 */
FingerManagerWindow::~FingerManagerWindow() {
	if (user) delete user;
	for (int i = 0; i < MAX_FINGERS; i++) {
		if (fingers[i]) delete fingers[i];
	}
	if (dbc) dbc->releaseConn();
	if (settings) delete settings;
	if (btng) delete btng;
	if (devices) {
		delete devices;
	}
	if (deleteAllBtn) delete deleteAllBtn;
	if (handsLabel) delete handsLabel;
	if (separator) delete separator;
	if (devicesLayout) delete devicesLayout;
	if (handsPanel) delete handsPanel;
	if (handsLayout) delete handsLayout;
	if (mainPanelLayout) delete mainPanelLayout;
	if (box) delete box;
	if (mainLayout) delete mainLayout;
}
