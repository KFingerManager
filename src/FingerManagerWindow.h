/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef FINGERMANAGERWINDOW_H_
#define FINGERMANAGERWINDOW_H_

#include <QObject>
#include <KDialog>
#include <KSeparator>
#include <QCheckBox>
#include <QWidget>
#include <QImage>
#include <QLabel>
#include <QButtonGroup>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <KConfig>
#include <KCModule>
#include <QGroupBox>
#include <KAboutData>
#include <KUser>

#include "FMDbusConn.h"

#define MAX_FINGERS 10

typedef struct {
	int x;
	int y;
} pos;

class FingerManagerWindow : public KCModule {

	Q_OBJECT

	private slots:
		void fingerSelected(int finger);
		void deviceChanged(int idx);
		void deleteAll();
	private:
		KAboutData	*about;
		QLabel		*handsLabel;
		QButtonGroup 	*btng;
		QWidget		*handsPanel;
		QCheckBox	*fingers[MAX_FINGERS];
		QComboBox	*devices;
		KConfig		*settings;
		QHBoxLayout	*handsLayout;
		QVBoxLayout	*mainPanelLayout;
		QVBoxLayout	*mainLayout;
		QHBoxLayout	*devicesLayout;
		KPushButton	*deleteAllBtn;
		QLabel		*deviceLabel;
		KSeparator	*separator;
		QGroupBox	*box;
		FMDbusConn	*dbc;
		QString		login;
		KUser		*user;
		
		QCheckBox*	createCheckBox(int idx, int x, int y);
		void		initComponents();
		void		loadConfig(KConfig *config);
		void		retranslate();
		void		processFingers(QStringList fngrs);
		void 		enableAllFingers(bool enable);
	public:
		FingerManagerWindow(QWidget *parent, const QVariantList &);
		~FingerManagerWindow();
};

#endif /*FINGERMANAGERWINDOW_H_*/
