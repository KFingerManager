/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef ENROLL_ANIM_LABEL_
#define ENROLL_ANIM_LABEL_

#include <QLabel>
#include <QWidget>
#include <QMovie>

class SensorAnimLabel : public QLabel {
	Q_OBJECT

	private:
		QMovie *movie;
		
	public:
		SensorAnimLabel(QString sensor="swipe", QWidget *parent = 0);
		~SensorAnimLabel();
		void start();
		void stop();
};

#endif