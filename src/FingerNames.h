/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef FINGERNAMES_H_
#define FINGERNAMES_H_

typedef struct {
	const char* name;
	const char* iface;
} finger;

const finger fingerNames[10] = {
	{"Left little finger",	"left-little-finger"},
	{"Left ring finger",	"left-ring-finger"},
	{"Left middle finger",	"left-middle-finger"},
	{"Left index finger",	"left-index-finger"},
	{"Left thumb",		"left-thumb"},
	{"Right thumb",		"right-thumb"},
	{"Right index finger",	"right-index-finger"},
	{"Right middle finger",	"right-middle-finger"},
	{"Right ring finger",	"right-ring-finger"},
	{"Right little finger",	"right-little-finger"},
};

#endif /* FINGERNAMES_H_ */