/*

Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include "FingerManagerDevicesModel.h"

/**
 * FingerManagerDevicesModel - data model for combo box.
 */
FingerManagerDevicesModel::FingerManagerDevicesModel(QObject *parent) :
	QAbstractListModel(parent) {
}

/**
 * destructor
 */
FingerManagerDevicesModel::~FingerManagerDevicesModel() {
	devices.clear();
}

/**
 * setDefault - set default device
 * @param dev device
 */
void FingerManagerDevicesModel::setDefault(QString dev) {
	for (int i = 0; i < devices.size(); i++) {
		if (devices.at(i).device == dev) {
			default_ = i;
			return;
		}
	}
}

/**
 * getDefault - return default device index
 * @return int
 */
int FingerManagerDevicesModel::getDefault() {
	return(default_);
}

/**
 * addDevice - add new device to model
 * @param device device object path
 * @param name device name
 */
void FingerManagerDevicesModel::addDevice(QString device, QString name) {
	DeviceModel d;
	d.name = name;
	d.device = device;
	devices.push_back(d);
}

/**
 * rowCount - returns number of devices in model 
 * @return int
 */
int FingerManagerDevicesModel::rowCount(const QModelIndex&) const {
	return(devices.size());
}

/**
 * data - return data for selected row and role
 * @param index row index
 * @param role role
 * @return QVariant
 */
QVariant FingerManagerDevicesModel::data(const QModelIndex& index, int role) const {
	QVariant result;
	if (role == Qt::DisplayRole) {
		result = devices.at(index.row()).name;
	}
	return(result);
}

/**
 * getDeviceName - return device name
 * @param row row index
 * @return QString
 */
QString FingerManagerDevicesModel::getDeviceName(int row) {
	return(devices.at(row).name);
}

/**
 * getDevicePath - return device path
 * @param row row index
 * @return QString
 */
QString FingerManagerDevicesModel::getDevicePath(int row) {
	return(devices.at(row).device);
}

/**
 * getDeviceModel - return device model
 * @param int row index
 * @return DeviceModel
 */
DeviceModel FingerManagerDevicesModel::getDeviceModel(int row) {
	return(devices.at(row));
}